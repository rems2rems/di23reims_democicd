import express from 'express'
import methodOverride from 'method-override'
import { loadFromHiddenField } from './middlewares/methodOverride'
import { createRouter as createRecipesRouter } from './recipes/routes'

import { router } from './users/routes'

const createApp = ({recipesController}: {recipesController: any}) => {
    
    const app = express()
    
    app.set("views", "views")
    app.set("view engine", "ejs")
    
    app.use(express.static("public"))
    app.use(express.urlencoded({ extended: true }))
    app.use(methodOverride(loadFromHiddenField))
    
    app.use("/users",router)
    app.use("/recipes",createRecipesRouter(recipesController) as any)
    
    return app
}

export { createApp }