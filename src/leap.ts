export function isLeap(year: number): boolean {
    if(year < 1582) {
        throw new Error("The year must be after 1582")
    }
    return year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0)
}