import mongoose from 'mongoose'
import { createApp } from './createApp'
import { createController } from './recipes/controller'
import { dbService } from './recipes/dbService'
import { seedRecipes } from './recipes/model'

const recipesController = createController(dbService)
const app = createApp({recipesController})

app.get("/", (req, res) => {
    res.redirect("/recipes")
})

console.log("DB HOST", process.env.DB_HOST);
mongoose.connect(process.env.DB_HOST as string).then(() => {
    console.log("Connected to MongoDB!");
    dbService.findPopulars().then((recipes) => {
        if(recipes.length === 0) {
            console.log("Seeding db with initial recipes...");
            dbService.seed(seedRecipes())
        }
    })
    app.listen(3000, () => {
        console.log('My app listening on port 3000!')
    })
})