import { Router } from "express";

export const createRouter = (controller:any) => {

    const router = Router()
    
    router.get("/",controller.listRecipes )
    router.post("/",controller.addRecipe)
    router.get("/showAddRecipe",controller.showAddRecipe)
    router.post("/search",controller.search)
    return router
}
