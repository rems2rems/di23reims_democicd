import * as yup from 'yup';

export type Recipe = {
    _id: string,
    name: string,
    description: string,
    ingredients: string[],
    steps: string[],
    needsOven : boolean,
    hasExoticIngredients : boolean,
    needsSpecialTools : boolean,
    likes: number;
};

// Définition du schéma Yup pour la validation de Recipe
export const recipeSchema = yup.object().shape({
    _id: yup.string(),
    name: yup.string().required(),
    description: yup.string().required(),
    ingredients: yup.array().of(yup.string()),
    steps: yup.array().of(yup.string()),
    needsOven: yup.boolean().default(false),
    hasExoticIngredients: yup.boolean().default(false),
    needsSpecialTools: yup.boolean().default(false),
    likes: yup.number().default(0).required()
});

export const newRecipeSchema = yup.object().shape({
    name: yup.string().required(),
    description: yup.string().required(),
    ingredients: yup.array().of(yup.string()),
    steps: yup.array().of(yup.string()),
    needsOven: yup.boolean().default(false),
    hasExoticIngredients: yup.boolean().default(false),
    needsSpecialTools: yup.boolean().default(false),
    likes: yup.number().default(0)
});

export function seedRecipes(): Recipe[] {
    const recipes: Recipe[] = [
        {_id: "1", name: "Poulet Basquaise", description: "Poulet mijoté avec des légumes du Pays Basque.", ingredients: ["poulet", "poivrons", "tomates", "oignons"], steps: ["Dorer le poulet", "Ajouter les légumes et cuire", "Servir chaud"], needsOven: true, hasExoticIngredients: true, needsSpecialTools: true, likes: 15},
        {_id: "2", name: "Ratatouille", description: "Plat de légumes mijotés de Provence.", ingredients: ["tomates", "courgettes", "aubergines", "poivrons"], steps: ["Couper les légumes", "Faire revenir à la poêle", "Cuire à feu doux"], needsOven: true, hasExoticIngredients: false, needsSpecialTools: false, likes: 20},
        {_id: "3", name: "Bouillabaisse", description: "Soupe de poisson traditionnelle de Marseille.", ingredients: ["poisson", "fruits de mer", "tomates", "oignon"], steps: ["Cuire les oignons", "Ajouter le poisson et les fruits de mer", "Mijoter avec des tomates"], needsOven: false, hasExoticIngredients: false, needsSpecialTools: false, likes: 25},
        {_id: "4", name: "Tarte Tatin", description: "Tarte aux pommes caramélisées, renversée après cuisson.", ingredients: ["pommes", "pâte brisée", "sucre", "beurre"], steps: ["Caraméliser les pommes", "Couvrir avec la pâte", "Cuire au four et renverser"], needsOven: true, hasExoticIngredients: false, needsSpecialTools: false, likes: 30},
        {_id: "5", name: "Quiche Lorraine", description: "Tarte salée originaire de Lorraine.", ingredients: ["pâte brisée", "lardons", "crème fraîche", "œufs"], steps: ["Préparer la pâte", "Ajouter lardons et crème", "Cuire au four"], needsOven: true, hasExoticIngredients: false, needsSpecialTools: false, likes: 22},
        {_id: "6", name: "Couscous", description: "Plat de semoule avec viande et légumes, d'origine nord-africaine.", ingredients: ["semoule", "agneau", "carottes", "courgettes"], steps: ["Cuire la viande", "Ajouter les légumes et la semoule", "Mijoter ensemble"], needsOven: false, hasExoticIngredients: true, needsSpecialTools: false, likes: 18},
        {_id: "7", name: "Paella", description: "Riz safrané espagnol avec fruits de mer et viandes.", ingredients: ["riz", "poulet", "fruits de mer", "poivrons"], steps: ["Cuire le poulet", "Ajouter le riz et les fruits de mer", "Cuire jusqu'à absorption du liquide"], needsOven: false, hasExoticIngredients: true, needsSpecialTools: true, likes: 35},
        {_id: "8", name: "Carbonade flamande", description: "Ragoût de bœuf à la bière, typique du nord de la France et de la Belgique.", ingredients: ["bœuf", "bière", "oignons", "pain d'épices"], steps: ["Dorer le bœuf", "Ajouter bière et oignons", "Cuire lentement"], needsOven: false, hasExoticIngredients: false, needsSpecialTools: false, likes: 19},
        {_id: "9", name: "Chili con carne", description: "Plat épicé à base de viande de bœuf et de haricots, origine Tex-Mex.", ingredients: ["bœuf", "haricots rouges", "tomates", "piments"], steps: ["Cuire le bœuf", "Ajouter haricots et tomates", "Mijoter et épicer"], needsOven: false, hasExoticIngredients: false, needsSpecialTools: false, likes: 28},
        {_id: "10", name: "Moussaka", description: "Gratin d'aubergines et de viande hachée, spécialité grecque.", ingredients: ["aubergines", "viande hachée", "tomates", "béchamel"], steps: ["Faire frire les aubergines", "Superposer avec la viande et la sauce", "Cuire au four"], needsOven: true, hasExoticIngredients: false, needsSpecialTools: false, likes: 30},
        {_id: "11", name: "Soupe Pho", description: "Bouillon vietnamien aux herbes, viande et nouilles de riz.", ingredients: ["bouillon de bœuf", "nouilles de riz", "viande de bœuf", "herbes"], steps: ["Préparer le bouillon", "Cuire les nouilles et la viande", "Servir avec des herbes fraîches"], needsOven: false, hasExoticIngredients: true, needsSpecialTools: false, likes: 40},
        {_id: "12", name: "Pad Thai", description: "Nouilles sautées thaïlandaises avec crevettes et cacahuètes.", ingredients: ["nouilles de riz", "crevettes", "œufs", "cacahuètes"], steps: ["Faire sauter les crevettes", "Ajouter les nouilles et les œufs", "Garnir de cacahuètes"], needsOven: false, hasExoticIngredients: true, needsSpecialTools: false, likes: 25},
        {_id: "13", name: "Lasagnes à la Bolognaise", description: "Couches de pâtes avec sauce à la viande et béchamel.", ingredients: ["pâtes à lasagne", "viande hachée", "sauce tomate", "béchamel"], steps: ["Préparer les sauces", "Monter les couches de lasagne", "Cuire au four"], needsOven: true, hasExoticIngredients: false, needsSpecialTools: false, likes: 50},
        {_id: "14", name: "Curry de poulet", description: "Poulet mijoté dans une sauce curry épicée.", ingredients: ["poulet", "lait de coco", "pâte de curry", "légumes"], steps: ["Dorer le poulet", "Ajouter la pâte de curry et le lait de coco", "Cuire avec des légumes"], needsOven: false, hasExoticIngredients: true, needsSpecialTools: false, likes: 32},
        {_id: "15", name: "Feijoada", description: "Ragoût brésilien de haricots noirs avec de la viande de porc.", ingredients: ["haricots noirs", "viande de porc", "saucisses", "légumes"], steps: ["Cuire les haricots et la viande", "Mélanger avec des légumes", "Mijoter ensemble"], needsOven: false, hasExoticIngredients: false, needsSpecialTools: false, likes: 18},
        {_id: "16", name: "Salade Niçoise", description: "Salade composée typique de Nice avec thon, œufs et légumes.", ingredients: ["thon", "œufs durs", "olives", "légumes variés"], steps: ["Cuire les œufs", "Préparer les légumes", "Assembler la salade"], needsOven: false, hasExoticIngredients: false, needsSpecialTools: false, likes: 22},
        {_id: "17", name: "Sushi", description: "Rouleaux de riz japonais avec poisson cru et légumes.", ingredients: ["riz sushi", "poisson cru", "algues", "légumes"], steps: ["Préparer le riz", "Couper le poisson", "Rouler les sushis"], needsOven: false, hasExoticIngredients: true, needsSpecialTools: true, likes: 45},
        {_id: "18", name: "Gazpacho", description: "Soupe froide espagnole à base de tomates et légumes crus.", ingredients: ["tomates", "concombre", "poivron", "ail"], steps: ["Couper les légumes", "Mixer jusqu'à obtenir une soupe", "Servir froid"], needsOven: false, hasExoticIngredients: false, needsSpecialTools: false, likes: 20},
        {_id: "19", name: "Beef Bourguignon", description: "Ragoût français de bœuf mijoté au vin rouge avec des champignons et des oignons.", ingredients: ["bœuf", "vin rouge", "champignons", "oignons"], steps: ["Dorer le bœuf", "Ajouter vin et légumes", "Mijoter longuement"], needsOven: false, hasExoticIngredients: false, needsSpecialTools: false, likes: 30},
        {_id: "20", name: "Tiramisu", description: "Dessert italien à couches de biscuits imbibés de café et crème au mascarpone.", ingredients: ["biscuits à la cuillère", "café fort", "mascarpone", "cacao en poudre"], steps: ["Imbiber les biscuits de café", "Alterner couches de biscuits et de crème mascarpone", "Saupoudrer de cacao"], needsOven: false, hasExoticIngredients: false, needsSpecialTools: false, likes: 55}
    ];
    return recipes;
}

// export function withLikes(recipes : Recipe[]) : Recipe[] {
//     return recipes.map((recipe) => ({...recipe, likes: Math.floor(Math.random() * 100)}));
// }