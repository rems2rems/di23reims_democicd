import { Recipe as DbRecipe } from "./mongo";
import { Recipe as ModelRecipe, seedRecipes } from "./model";

export const dbService = {
  seed: async (data: ModelRecipe[] | undefined) => {
    DbRecipe.insertMany(
      seedRecipes().map((recipe) => {
        const r: any = { ...recipe };
        delete r._id;
        return r;
      })
    );
  },
  findById: async (id: string) => DbRecipe.findById(id),
  save: async (recipe: ModelRecipe) => new DbRecipe(recipe).save(),
  remove: async (_id: string) => DbRecipe.deleteOne({ _id }),
  findPopulars: async (nb : number = 10) : Promise<ModelRecipe[]> =>{
    let populars = await DbRecipe.find().limit(100);
    populars.sort((recipe1, recipe2) => recipe2.likes - recipe1.likes);
    return populars.slice(0, nb);
  },
  findByText: async (text: string) => {
    return DbRecipe.find({
      $text: { $search: text },
    }).limit(100);
  },
  addLike: async (_id: string) => {
    const recipe = await DbRecipe.findById(_id);
    if (!recipe) {
      throw new Error("Recipe not found");
    }
    recipe.likes += 1;
    recipe.save();
  },
  removeLike: async (_id: string) => {
    const recipe = await DbRecipe.findById(_id);
    if (!recipe) {
      throw new Error("Recipe not found");
    }
    if (recipe.likes > 0) {
      recipe.likes -= 1;
      recipe.save();
    }
  },
};