import { Recipe } from "./model"

export type Difficulty = "easy" | "medium" | "hard"
export function createDifficulty(recipe : Recipe) : Difficulty{
    if(recipe.needsOven && recipe.hasExoticIngredients && recipe.needsSpecialTools) {
        return "hard"
    }
    if(recipe.needsOven || recipe.hasExoticIngredients || recipe.needsSpecialTools) {
        return "medium"
    }
    return "easy"
}