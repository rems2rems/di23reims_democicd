import { Recipe, recipeSchema } from "./model";
import { dbService as DbService } from "./dbService";

function createController(dbService : typeof DbService) {
  const controller = {} as any;

  controller.listRecipes = async function listRecipes(req: any, res: any) {
    
    const recipes = await dbService.findPopulars();
    res.header("Content-Type", "text/html");
    res.render("recipes/listRecipes", { recipes,search: "" });
  };

  controller.showAddRecipe = function showAddRecipe(req: any, res: any): void {
    res.render("recipes/addRecipe");
  }

  controller.addRecipe = async function addRecipe(req: any, res: any,next: any): Promise<void> {

    try {

        try {
          const recipe = await recipeSchema.validate(req.body) as Recipe
          dbService.save(recipe);
          res.redirect("/recipes");
        } catch ({errors} : any) {
          res.render("recipes/addRecipe", { recipe:req.body,errors });
        }
    }
    catch({errors} : any){
        next(errors[0].message)
        return
    }
    
  };
  controller.likeRecipe = async function likeRecipe(req: any, res: any): Promise<void> {
      const _id = req.params._id;
      await dbService.addLike( _id);
      res.redirect("/recipes");
  }
  
  controller.removeRecipe = async function removeRecipe(req: any, res: any): Promise<void> {
    const _id = req.params._id;
    await dbService.remove(_id);
    res.redirect("/recipes");
  }

  controller.search = async function search(req: any, res: any): Promise<void> {
    const search = req.body.search;
    
    let recipes
    if(!!search){
      recipes = await dbService.findByText(search);
    }
    else{
      recipes = await dbService.findPopulars();
    }

    res.header("Content-Type", "text/html");
    res.render("recipes/listRecipes", { recipes, search });
  }
  return controller;
}

export { createController };

