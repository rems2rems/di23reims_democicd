import { Schema, model } from "mongoose";
import { Recipe as RecipeModel } from "./model"

export const recipeSchema = new Schema({
    name: String,
    description: String,
    ingredients: [String],
    steps: [String],
    needsOven : Boolean,
    hasExoticIngredients : Boolean,
    needsSpecialTools : Boolean,
    likes: {type: Number, default: 0},
})
recipeSchema.index({ name: 'text', description: 'text' });

export const Recipe = model<RecipeModel>("Recipe", recipeSchema)