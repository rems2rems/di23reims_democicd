import { User as ModelUser} from "./models"

const users : ModelUser[] = []

export const User = (data : any) => {

    data._id = users.length
    return {
        save : () => { users[data._id] = data },
    }
}

User.find = (object : {_id:any}) : ModelUser[] => {return []}
User.findOne = (object : {_id:any}) : ModelUser => {return users[object._id]!}
