import { RequestHandler } from "express"
import { User } from "./models"
import { User as DbUser } from "./array-persistence"

const users : User[] = []

export const showAddUserForm : RequestHandler= (req, res) => {
    res.render("addUser")
}

export const showUpdateUserForm : RequestHandler = (req, res) => {
    const idx : number = parseInt(req.params.idx)
    const user = users[idx]
    res.render("updateUser", { user ,idx })
}


export const showUser : RequestHandler =  (req, res) => {
    
    const _id = req.params.idx
    const user = DbUser.findOne({_id})
    res.render("user", { user })
}

export const listUsers : RequestHandler = (req, res) => {
    
    res.render("users", { users })
}

export const addUser : RequestHandler = (req, res) => {
    users.push(req.body as User)
    res.redirect("/users")
}


export const updateUser : RequestHandler = (req, res) => {
    const idx : number = parseInt(req.params.idx)
    users[idx] = req.body as User
    res.redirect("/users")
}

export const deleteUser : RequestHandler = (req, res) => {
    const idx : number = parseInt(req.params.idx)
    users.splice(idx, 1)
    res.redirect("/users")
}