export type User = {
    _id : string | null ,
    firstName: string | null
    lastName: string | null,
    dob : Date | null
}