import { User } from "./models";

export function isValid(user : User) {
    return !!user.firstName && !!user.lastName
}

export const isAdult = (user : User) => {
    return !!user.dob && (new Date().getFullYear() - user.dob.getFullYear()) >= 18
}