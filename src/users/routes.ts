import { Router } from "express";
import { addUser, deleteUser, listUsers, showAddUserForm, showUpdateUserForm, showUser, updateUser } from "./controllers-array";

export const router = Router()

router.get("/add", showAddUserForm)
router.get("/:idx/update", showUpdateUserForm)
router.get("/:idx", showUser)
router.get("/", listUsers)
router.post("/", addUser)
router.put("/:idx", updateUser)
router.delete("/:idx", deleteUser)