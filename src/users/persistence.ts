import { Model, Schema, model } from "mongoose"
import { User as ModelUser} from "./models"

export const UsersSchema = new Schema<ModelUser>({
    firstName : String,
    lastName : String
})

export const User = model<ModelUser>("User", UsersSchema)