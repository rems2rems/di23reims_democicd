import { describe, expect, it } from "vitest";
import { isLeap } from "../src/leap";

describe('a leap year test', () => {
    it('must return true for 1900', () => {
        expect(isLeap(1900)).toBe(false);
    })
    it('must return true for 2019', () => {
        expect(isLeap(2019)).toBe(false);
    })
    it('must return true for 2000', () => {
        expect(isLeap(2000)).toBe(true);
    })
    it('must return true for 2020', () => {
        expect(isLeap(2020)).toBe(true);
    })
})

describe('a leap year test incorrect call', () => {
    it('must throw an error for a year before 1582', () => {
        expect(() => isLeap(1581)).toThrow();
    })
})