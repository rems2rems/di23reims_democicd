import { describe, expect, it } from "vitest";
import { dbService } from "../../../src/recipes/dbService";
import { Recipe as DbRecipe } from "../../../src/recipes/mongo";
import { seedRecipes } from "../../../src/recipes/model";
import sinon from "sinon";

describe('popular recipes', () => {
    it('Should have more likes then others', async () => {

        let recipes = seedRecipes()
        const find = sinon.stub(DbRecipe,"find").returns({limit: () => {return Promise.resolve(recipes)}} as any)
        const populars = await dbService.findPopulars()
        const notPopulars = recipes.filter((recipe) => !populars.map((popular) => popular._id).includes(recipe._id))
        const maxNotPopular = notPopulars.map((recipe) => recipe.likes).reduce((a, b) => Math.max(a, b), 0)
        
        expect(populars).toBeTruthy();
        expect(Array.isArray(populars)).toBe(true)
        expect(populars.length).toBe(10)
        expect(populars[0].likes).toBeGreaterThan(populars[1].likes)
        expect(populars[1].likes).toBeGreaterThan(populars[2].likes)
        for(const popular of populars){
            expect(popular.likes).toBeGreaterThanOrEqual(maxNotPopular)
        }
    });
});