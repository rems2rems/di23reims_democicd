import { stub } from "sinon";
import request from "supertest";
import { describe, expect, it } from "vitest";
import { createApp } from '../../../src/createApp';
import {createController} from "../../../src/recipes/controller";
import { dbService } from "../../../src/recipes/dbService";

describe('recipes routes', () => {

    it('must have a route to list recipes', async () => {

        const recipesController = createController(dbService)
        const listRecipes = stub(recipesController,"listRecipes").callsFake((req,res : any)=>{
            res.status(200).send()
        })
        const client = request(createApp({recipesController}))
        const res = await client.get('/recipes')  
                        
        expect(res.status).toBe(200)
                    
        expect(listRecipes.callCount).toBe(1)
    })
})