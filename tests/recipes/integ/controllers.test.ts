import { createResponse } from "node-mocks-http";
import sinon from "sinon";
import { describe, expect, it } from "vitest";
import { createController } from "../../../src/recipes/controller";
import { dbService } from '../../../src/recipes/dbService';
import { seedRecipes } from "../../../src/recipes/model";

describe('recipes controllers', () => {
    it('must have a controller to list recipes', async () => {

        const response = createResponse()
        const render = sinon.stub(response,"render")
        const findPops = sinon.stub(dbService,"findPopulars").returns(Promise.resolve(seedRecipes().slice(0,10)))
        const recipesController = createController(dbService)
        await recipesController.listRecipes(null,response)
        expect(response.statusCode).toBe(200)

        expect(findPops.calledOnce).toBe(true)
        
        expect(render.calledOnce).toBe(true)
        const args = render.firstCall.args
        expect(args[0]).toBe("recipes/listRecipes")
        let recipes = (args[1]! as any).recipes
        expect(recipes).toBeTruthy()
        expect(Array.isArray(recipes)).toBe(true)
        expect(recipes.length).toBe(10)

        render.restore()
        findPops.restore()
    })
})