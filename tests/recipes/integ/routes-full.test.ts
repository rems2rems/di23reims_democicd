import { JSDOM } from "jsdom";
import sinon from "sinon";
import request from "supertest";
import { describe, expect, it } from "vitest";
import { createApp } from '../../../src/createApp';
import { createController } from "../../../src/recipes/controller";
import { dbService } from "../../../src/recipes/dbService";
import { seedRecipes } from "../../../src/recipes/model";

describe('recipes routes with controllers', () => {

    it('must have a route to list recipes', async () => {

        const findPops = sinon.stub(dbService,"findPopulars").returns(Promise.resolve(seedRecipes().slice(0,10)))
        const recipesController = createController(dbService)
        const client = request(createApp({recipesController}))
        const res = await client.get('/recipes')  
                        
        expect(res.status).toBe(200)
        const {window:{document}} = new JSDOM(res.text)
        const body = document.body
        expect(document.title).toBe("Recipes")        
        // const list = getByRole(body, "list")
        // expect(list.nodeName).toBe("UL")
        // expect(list.children.length).toBe(1)
        // expect(list.children[0].nodeName).toBe("LI") 
        // expect(list.children[0].textContent).toBe("Boeuf bourguignon à la réunionaise")
        
        expect(findPops.calledOnce).toBe(true)
        findPops.restore()
    })
})