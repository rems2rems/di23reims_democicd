import { describe, expect, it } from "vitest";
import { JSDOM } from "jsdom"
import {getByRole,getByText,getByTitle} from "@testing-library/dom"
import {Data, renderFile} from "ejs"
import path from "node:path";
import { seedRecipes } from "../../../src/recipes/model";

describe('recipes controllers', () => {
    it('must have a controller to list recipes', async () => {

        const template = path.resolve("views/recipes/listRecipes.ejs")
        const page = await renderFile(template,{recipes:seedRecipes()} as Data) 
               
        const {window:{document}} = new JSDOM(page)
        const body = document.body
        expect(document.title).toBe("Recipes")
        const h1 = body.querySelector("h1")
        expect(h1).toBeTruthy()
        expect(h1!.textContent).toBe("Recipes:")  
        const pageTitle = getByText(body,"Recipes:")
        expect(pageTitle).toBeTruthy()
        const recipes = Array.from(pageTitle.parentNode?.querySelectorAll("article") || [])
        expect(recipes.length).toBe(20)
        const first = recipes[0]
        expect(first).toBeTruthy()
        const header = first.querySelector("header")!
        expect(header).toBeTruthy()
        expect(header.textContent?.trim()).toBe("Poulet Basquaise")
        const descr = first.querySelector("main")!
        expect(descr).toBeTruthy()
        expect(descr.textContent?.trim()).toBe("Poulet mijoté avec des légumes du Pays Basque.")
        const nbLikes = first.querySelector("footer")!        
        expect(nbLikes).toBeTruthy()
        expect(nbLikes.textContent?.trim()).toBe("15")
    })
})