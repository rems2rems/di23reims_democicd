import { describe, expect, it } from "vitest";
import { createDifficulty } from "../../../src/recipes/difficulty";
import { seedRecipes } from "../../../src/recipes/model";

describe('a recipe difficulty', () => {
    it('must be difficult with 3 criterias', () => {

        const recipe = seedRecipes()[0]
        expect(createDifficulty(recipe)).toBe("hard")
    })
    it('must be medium with 1 criterias', () => {

        const recipe = seedRecipes()[1]
        expect(createDifficulty(recipe)).toBe("medium")
    })
    it('must be easy with 0 criteria', () => {

        const recipe = seedRecipes()[2]
        expect(createDifficulty(recipe)).toBe("easy")
    })
})