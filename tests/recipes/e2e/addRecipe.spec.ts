import { test, expect } from '@playwright/test';

test('add new recipe without description', async ({ page }) => {
  await page.goto('http://localhost:3000');
  await page.getByRole('link', { name: 'Add a new recipe' }).click();
  await page.getByRole('button', { name: 'Add recipe' }).click();
  expect(await page.getByText('description is a required field')).toBeTruthy();
});

test('add new recipe without name', async ({ page }) => {
    await page.goto('http://localhost:3000');
    await page.getByRole('link', { name: 'Add a new recipe' }).click();
    await page.getByLabel('Description').click();
    await page.getByLabel('Description').fill('une description');
    await page.getByRole('button', { name: 'Add recipe' }).click();
    expect(await page.getByText('name is a required field')).toBeTruthy();
  });

test('add new recipe with name and description', async ({ page }) => {
    await page.goto('http://localhost:3000');
    await page.getByRole('link', { name: 'Add a new recipe' }).click();
    await page.getByRole('button', { name: 'Add recipe' }).click();
    await page.getByLabel('Name').click();
    await page.getByLabel('Name').fill('un nom');
    await page.getByLabel('Description').click();
    await page.getByLabel('Description').fill('une description');
    await page.getByRole('button', { name: 'Add recipe' }).click();
    expect(await page.getByRole('heading', { name: 'Recipes:' })).toBeTruthy();
  });