import { expect, describe, it, beforeEach } from "vitest";
import { isAdult, isValid } from "../src/users/business";

describe('check user isValid', () => {

    it('must be true', () => {
        const user = { _id:null,firstName: "John", lastName: "Doe",dob:null }
        expect(isValid(user)).toBe(true);
    });

    it('must be false', () => {
        const user = { _id:null,firstName: "John", lastName: null,dob:null }
        expect(isValid(user)).toBe(false);
    });
});

describe('check user isAdult', () => {

    it('must be true', () => {
        const yearOfBirth = new Date().getFullYear() - 16
        const user = { _id:null,firstName: "John", lastName: null,dob:new Date(yearOfBirth,1,1) }
        expect(isAdult(user)).toBe(false);
    });

    it('must be false', () => {
        const yearOfBirth = new Date().getFullYear() - 19
        const user = { _id:null,firstName: "John", lastName: null,dob:new Date(yearOfBirth,1,1) }
        expect(isAdult(user)).toBe(true);
    });
});